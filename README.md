# busy wait loop

A generic implementation of a busy wait loop in Python using the time module to keep track of loop duration.

Busy waiting is considered an anti pattern, but for embedded systems that are not time critical it is often appropriate when you need events to happen in specific intervals and free the rest of the time for doing other stuff, like i/o handling.

Wikipedia has a nice article: [https://en.wikipedia.org/wiki/Busy_waiting](https://en.wikipedia.org/wiki/Busy_waiting)

This code is explained in further detail: [https://eal-itt.gitlab.io/discover-iot/system/code_flow](https://eal-itt.gitlab.io/discover-iot/system/code_flow)

# Development usage

You need Python above version 3.5

1. Clone the repository `git clone git@gitlab.com:npes-py-experiments/busy-wait-loop.git` 
2. Create a virtual environment https://packaging.python.org/guides/installing-using-pip-and-virtual-environments/#creating-a-virtual-environment 
2. Activate the virtual environment https://packaging.python.org/guides/installing-using-pip-and-virtual-environments/#activating-a-virtual-environment
4. Run `python3 <appname.py>` (linux) or `py <appname.py>` (windows)