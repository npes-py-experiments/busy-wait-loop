import time # used to get timestamp

# define functions
def now_ms():
    ms = time.perf_counter()
    # print(ms)
    return ms

# initialize variables
loop_duration = 15 # in milliseconds
loop_start = now_ms()

while True:
    if (now_ms() - loop_start > loop_duration):
        # send values to Thingspeak here
        print('sending to Thingspeak')
        loop_start = now_ms()
        continue # continue to top of while loop
    else:
        # read senors, buttons and update led's here
        time.sleep(1) # simulates some operations that takes 1 second (remove in live system)
        print(f'loop elapsed ms {now_ms() - loop_start}') # print loop time