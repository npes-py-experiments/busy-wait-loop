import time # used to get timestamp

# define functions
def now_ms():
    ms = int(time.time() * 1000)
    return ms

def button_press(value):
    value = value + 1
    return value

def send_thingspeak(values):
    print('sending to Thingspeak')
    print(values[0])
    values[0] = 0
    return values


# initialize variables
loop_duration = 15000 # in milliseconds
loop_start = now_ms()
btn1_count = 0

while True:
    if (now_ms() - loop_start > loop_duration):
        # send values to Thingspeak here
        btn1_count = send_thingspeak(btn1_count)
        loop_start = now_ms()
        continue # continue to top of while loop
    else:
        # read senors, buttons and update led's here
        time.sleep(1) # simulates some operations that takes 1 second (remove in live system)
        print(f'loop elapsed ms {now_ms() - loop_start}') # print loop time
        btn1_count = button_press(btn1_count)