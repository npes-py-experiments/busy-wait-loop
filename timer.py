"""
Written by Nikolaj Simonsen - 2021.11.28
Modified version of https://realpython.com/python-timer/ 
"""

# timer.py

from dataclasses import dataclass, field
import time
from typing import Callable, ClassVar, Dict, Optional

class TimerError(Exception):
    """A custom exception used to report errors in use of Timer class"""

@dataclass
class Timer:
    timers: ClassVar[Dict[str, float]] = dict()
    name: Optional[str] = None
    text: str = "Elapsed time: {:0.4f} seconds"
    logger: Optional[Callable[[str], None]] = print
    _start_time: Optional[float] = field(default=None, init=False, repr=False)

    def __post_init__(self) -> None:
        """Add timer to dict of timers after initialization"""
        if self.name is not None:
            self.timers.setdefault(self.name, 0)

    def start(self) -> None:
        """Start a new timer"""
        if self._start_time is not None:
            raise TimerError(f"Timer is running. Use .stop() to stop it")

        if self.logger:
            self.logger(f'{self.name} started')

        self._start_time = time.perf_counter()

    def stop(self) -> float:
        """Stop the timer, and report the elapsed time"""
        if self._start_time is None:
            raise TimerError(f"Timer, {self.name}, is not running. Use .start() to start it")

        # Calculate elapsed time
        elapsed_time = time.perf_counter() - self._start_time
        self._start_time = None

        # Report elapsed time
        if self.logger:
            self.logger(f'{self.name} stopped, {self.text.format(elapsed_time)}')
        if self.name:
            self.timers[self.name] += elapsed_time

        return elapsed_time

    def reset(self) -> float:


        if self._start_time is None:
            raise TimerError(f"Timer is not running. Use .start() to start it")

        # Reset timer
        self._start_time = time.perf_counter()


    def elapsed(self) -> float:
        """returns elapsed time"""

        if self._start_time is None:
            raise TimerError(f"Timer is not running. Use .start() to start it")

        # Calculate elapsed time
        elapsed_time = time.perf_counter() - self._start_time

        # Report elapsed time
        if self.logger:
            self.logger(f'{self.name} {self.text.format(elapsed_time)}')
        if self.name:
            self.timers[self.name] += elapsed_time

        return elapsed_time
