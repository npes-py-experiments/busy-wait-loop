"""
Written by Nikolaj Simonsen - 2021.11.28
"""

# IMPORT LIBS

from timer import Timer
import time
import logging

# INIT TIMER OBJECTS
timer1 = Timer(name='timer1', logger=logging.warning)
timer2 = Timer(name='timer2', logger=logging.warning)

# START TIMERS
timer1.start()
timer2.start()

# SET LOOP DURS
loop_duration1 = 15 # in seconds
loop_duration2 = 5 # in seconds

while True:
    try:
        if (timer1.elapsed() > loop_duration1):
            # send values to Thingspeak here
            print('sending to Thingspeak')
            timer1.reset()

        elif (timer2.elapsed() > loop_duration2):
            # send values to Thingspeak here
            print('doing something every 5 seconds with timer 2')
            timer2.reset()

        else:
            # read senors, buttons and update led's here
            time.sleep(1) # simulates some operations that takes 1 second (remove in live system)

    except KeyboardInterrupt:
        timer1.stop()
        timer2.stop()
        logging.critical('program terminated by user.......')
        exit(0)